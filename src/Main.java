
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Scanner;

public class Main {

    public void mainMenu() {
        System.out.println("\n================(MENU-LIST)================");
        System.out.println("1 . Add Employee ");
        System.out.println("2 . Edit Employee");
        System.out.println("3 . Remove Employee");
        System.out.println("4 . Exit ");
        System.out.println("========================================");
    }

    public void subMenu() {
        System.out.println("================(OPTIONS)===============");
        System.out.println("1 . Volunteer Employee");
        System.out.println("2 . Hourly Employee");
        System.out.println("3 . Salaried Employee");
        System.out.println("4 . Back");
    }

    public String NameUpterCasde(String str) {
        String output = str.substring(0, 1).toUpperCase() + str.substring(1);
        return output;
    }

    public static void main(String[] args) {

        boolean check = false;
        Main emp = new Main();
        ArrayList<StuffMember>stuff = new ArrayList<StuffMember>();
        stuff.add(new Volunteer(1,"Koko","Kep"));
        stuff.add(new SalariedEmployee(2,"Bopha","PP",400,100));
        stuff.add(new HourlyEmployee(3,"seth","KompongCham",30,10));

        Scanner sc = new Scanner(System.in);

        while (true) {
            Collections.sort(stuff, new SortName());
            for (StuffMember sm: stuff){
                System.out.println(sm);
            }
            A:
            emp.mainMenu();
            System.out.print("=>Please input any Option : ");
            String option = sc.next();

            switch (option) {

                case "1": {
                    do {
                        emp.subMenu();
                        System.out.print("=>Choose type of Employee : ");
                        String subOption = sc.next();
                        System.out.println("\n----------------------------------------");

                        switch (subOption) {
                            case "1":
                                System.out.print("Input Employee's Id      : ");
                                int id = sc.nextInt();
                                System.out.print("Input Employee's Name    : ");
                                sc.nextLine();
                                String name = sc.nextLine();
                                System.out.print("Input Employee's Address : ");
                                String address = sc.nextLine();
                                System.out.println("\n");

                                stuff.add(new Volunteer(id, emp.NameUpterCasde(name), address));
                                check = true;
                                break;
                            case "2":
                                System.out.print("Input Employee's Id      : ");
                                int idHour = sc.nextInt();
                                System.out.print("Input Employee's Name    : ");
                                sc.nextLine();
                                String nameHour = sc.nextLine();
                                System.out.print("Input Employee's Address : ");
                                String addressHour = sc.nextLine();
                                System.out.print("Input Employee's Hours   : ");
                                int hours = sc.nextInt();
                                System.out.print("Input Employee's Rate    : ");
                                double rate = sc.nextDouble();

                                stuff.add(new HourlyEmployee(idHour, emp.NameUpterCasde(nameHour), addressHour, hours, rate));
                                check = true;
                                break;
                            case "3":
                                System.out.print("Input Employee's Id      : ");
                                int idSala = sc.nextInt();
                                System.out.print("Input Employee's Name    : ");
                                sc.nextLine();
                                String nameSala = sc.nextLine();
                                System.out.print("Input Employee's Address : ");
                                String addressSala = sc.nextLine();
                                System.out.print("Input Employee's Salary  : ");
                                double salary = sc.nextDouble();
                                System.out.print("Input Employee's Bonus   : ");
                                double bonus = sc.nextDouble();

                                stuff.add(new SalariedEmployee(idSala, emp.NameUpterCasde(nameSala), addressSala, salary, bonus));
                                check = true;
                                break;
                            case "4":
                                check = false;
                                continue;
                            default:
                                System.out.println("***Please input valid Options!");
                                System.out.println("--------------------------------");
                        }
                        Collections.sort(stuff, new SortName());

                        for (StuffMember sm: stuff){
                            System.out.println(sm);
                        }
                    } while (check != false);
                    break;
                }
                case "2": {

                    System.out.print("Please input Employee's ID to Edit : ");
                    int idUpdate = sc.nextInt();
                    System.out.println("\n");

                    for (StuffMember update : stuff) {
                        if (update.getID() == idUpdate) {
                            System.out.println(update);
                            if ("employee.Volunteer".equals(update.getClass().getName())) {

                                System.out.print("Input Employee's Name    : ");
                                sc.nextLine();
                                String name = sc.nextLine();
                                System.out.print("Input Employee's Address : ");
                                String address = sc.nextLine();
                                System.out.println("\n");

                                update.setName(emp.NameUpterCasde(name));
                                update.setAddress(address);

                            } else if ("employee.SalariesEmployee".equals(update.getClass().getName())) {
                                System.out.print("Input Employee's Name    : ");
                                sc.nextLine();
                                String name = sc.nextLine();
                                System.out.print("Input Employee's Address : ");
                                String address = sc.nextLine();
                                System.out.print("Input Employee's Salaries : ");
                                double salary = sc.nextDouble();
                                System.out.print("Input Employee's Bonus    : ");
                                double bonus = sc.nextDouble();
                                System.out.println("\n");

                                update.setName(emp.NameUpterCasde(name));
                                update.setAddress(address);
                                ((SalariedEmployee) update).setSalary(salary);
                                ((SalariedEmployee) update).setBonus(bonus);

                            } else if ("employee.HourlyEmployee".equals(update.getClass().getName())) {
                                System.out.print("Input Employee's Name    : ");
                                sc.nextLine();
                                String name = sc.nextLine();
                                System.out.print("Input Employee's Address : ");
                                String address = sc.nextLine();
                                System.out.print("Input Employee's Hours : ");
                                int hours = sc.nextInt();
                                System.out.print("Input Employee's Rate    : ");
                                double Rate = sc.nextDouble();
                                System.out.println("\n");

                                update.setName(emp.NameUpterCasde(name));
                                update.setAddress(address);
                                ((HourlyEmployee) update).setHourWorked(hours);
                                ((HourlyEmployee) update).setRate(Rate);

                            }
                        }
                    }
                    break;
                }
                case "3": {
                    System.out.print("=>Input id you want to delete : ");
                    int delelteId = sc.nextInt();

                    Iterator<StuffMember> itr = stuff.iterator();
                    while (itr.hasNext()) {
                        StuffMember deleteEmp = itr.next();

                        if (deleteEmp.getID() == delelteId) {
                            itr.remove();
                        }
                    }
                    break;
                }
                case "4": {
                    System.out.println("==================(BYE-BYE)==================\n");
                    System.exit(0);
                    break;
                }
                default: {
                    System.out.print("***Please input valid number");
                    break;
                }
            }
        }
    }
}
