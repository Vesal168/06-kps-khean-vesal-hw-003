

public class SalariedEmployee extends StuffMember{

    private double salary, bonus;

    public void setSalary(double salary){
        this.salary=salary;
    }
    public double getSalary(){
        return salary;
    }
    public void setBonus(double bonus){
        this.bonus=bonus;
    }
    public double getBonus(){
        return bonus;
    }

    public SalariedEmployee(int id, String name, String address, double salary, double bonus) {
        super(id,name,address);
        this.salary=salary;
        this.bonus=bonus;
    }

    @Override
    public String toString(){
        return  "Id      : "+ id+"\n"+
                "Name    : "+ name+"\n"+
                "Address : "+address+"\n"+
                "Salary  : "+ salary+"\n"+
                "Bonus   : "+bonus+"\n"+
                "Payment : "+pay()+"\n"+
                "-------------------\n";
    }

    @Override
    double pay() {
        return salary+bonus;
    }

}
