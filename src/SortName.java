
import java.util.Comparator;

public class SortName implements Comparator<StuffMember> {

    @Override
    public int compare(StuffMember od1, StuffMember od2) {
        return od1.getName().compareTo(od2.getName());
    }

}
