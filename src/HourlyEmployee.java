
public class HourlyEmployee extends StuffMember {

    private int hourWorked;
    private double rate;

    public void setHourWorked(int hourWorked){
        this.hourWorked=hourWorked;
    }
    public int getHourWorked(){
        return hourWorked;
    }
    public void setRate(double rate){
        this.rate=rate;
    }
    public double getRate(){
        return rate;
    }

    public HourlyEmployee(int id, String name, String address,int hourWorked,double rate) {
        super(id, name, address);
        this.hourWorked=hourWorked;
        this.rate=rate;
    }

    public String toString(){
        return  "Id      : "+ id+"\n"+
                "Name    : "+ name+"\n"+
                "Address : "+address+"\n"+
                "Hours   : "+ hourWorked+"\n"+
                "Rate    : "+ rate+"\n"+
                "Payment : "+ pay()+"\n"+
                "--------------------\n";
    }

    @Override
    double pay() {
        return hourWorked*rate;
    }
}
